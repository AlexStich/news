<!--<script type="text/javascript" src="script/ajax.js"></script>-->

<?php    
/* Инициализация БД */

    $host = 'localhost'; //имя хоста, на локальном компьютере это localhost
    $user = 'root'; //имя пользователя, по умолчанию это root
    $password = ''; //пароль, по умолчанию пустой
    $db_name = 'news'; //имя базы данных
    $id =0; // Id
    $title = ''; // Название статьи
    $summary = ''; // Краткое содежрание
    $content = ''; // Полное содержание
    $author = ''; // Автор
    $date = '2017-01-01 01:01:01'; // Дата, время
    $pp = 5; //Максимальное кол-во новостей на странице
    $prl = 3; // Кол-во страниц отображаемых в пагинаторе справа и слева
    $month_query = '';

/* Подключенние, проверка */

    $link = mysqli_connect ($host, $user, $password, $db_name);
    if (!$link){
        exit ('<br>Невозможно подключиться к базе.<br>');
    }

/* Установка параметров пагинации*/

    if (empty($_GET['page'])){$pn = 1;} else{$pn = $_GET['page'];}
    
    if (empty($_GET['month'])){$month = 0;} else{$month = $_GET['month'];}
    
    if (empty($pc)){
        if($month!==0){$month_query = " WHERE MONTH(date)=".$month;}
        $query = "SELECT COUNT(date) FROM article".$month_query;
        $result_for_count = $link -> query ($query);
        $ccount = mysqli_fetch_array($result_for_count);
        $pc = ceil ($ccount [0]/$pp);
    } else{
        exit ('<br>Невозможно определить количество записей в базе.<br>');    
    }

/* Расчет значений пагинатора */

    if ($pn > $prl && $prl < $pc-$pn){
        $first = $pn-$prl;
        $last = $pn+$prl;
    }elseif($pn>$prl && $prl >= $pc-$pn){
        $first = $pn-$prl;
        $last = $pc;
    }elseif($pn<=$prl && $prl < $pc-$pn){
        $first = 1;
        $last = $pn+$prl;
    }else{
        $first = 1;
        $last = $pc;
    }
?>

<!-- Пагинатор-->

    <nav>
      <ul class="pagination">
    <?php for ($i=$first;$i<=$last;$i++):?>
         <li><a href="#" data-key="<?= $i?>" <?= $i==$pn?' class="active-page"':' class="page"' ?>> <?= $i ?></a></li>
    <?php endfor;  
        if ($pc>$last): ?>
        <li><a href="#" data-key="<?= ++$last?>" class="page">Далее</a></li>
    <?php endif;
    
    ?>
      </ul>
    </nav>

<?php
    
/* Установка кодировки utf8 */

    $link -> set_charset ('utf8');

/* Запрос данных */
    
    $query = "SELECT id, title, summary, date FROM article".$month_query." ORDER BY date DESC LIMIT ".($pn-1)*$pp.",".$pp;
    $result = $link -> query ($query);

/* Преобразование в массив, вывод */

    for ($data = []; $row = mysqli_fetch_assoc($result); $data[] = $row){}

    foreach ($data as $key => $value){
        echo '<div class="overview">';
        echo '<a href="'.'article.php?id='.$value['id'].'"'.'class="overview_summary" method="GET">'.$value['summary'].'</a>';
        echo '<div class="overview_date">'.$value['date'].'</div>';
        echo '</div>';
    }
     if ($link){$link -> close();}
?>


