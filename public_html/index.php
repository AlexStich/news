<?php setcookie('month', 0); ?>

<!DOCTYPE html>
<html>
    
<head>
<title>Новости Екатеринбурга</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Description" content="News">
<link rel="stylesheet" href="css/style.css">
<script type="text/javascript" src="script/jquery-3.2.1.js"></script>
<script type="text/javascript" src="script/ajax.js"></script>
</head>

<body>

<div class="container">

    <nav>
        <ul class="menu-main">
            <li><a href="index.php" class="current">Главная</a></li>
            <li><a href="create_news.html">Создать новость</a></li>
            <li><a href="tz.html">Техзадание</a></li>
            <li><a href="https://bitbucket.org/AlexStich/news/src/e8a79d79479042b2af86b81d87496aaf5a1742dd/public_html/?at=master">Код в репозитарии</a></li>
        </ul>
        <ul class="menu-main">
            <li><a href="#" data-key="1" class="month">Январь</a></li>
            <li><a href="#" data-key="2" class="month">Февраль</a></li>
            <li><a href="#" data-key="3" class="month">Март</a></li>
            <li><a href="#" data-key="4" class="month">Апрель</a></li>
            <li><a href="#" data-key="5" class="month">Май</a></li>
            <li><a href="#" data-key="6" class="month">Июнь</a></li>
            <li><a href="#" data-key="7" class="month">Июль</a></li>
            <li><a href="#" data-key="8" class="month">Август</a></li>
            <li><a href="#" data-key="9" class="month">Сентябрь</a></li>
            <li><a href="#" data-key="10" class="month">Октябрь</a></li>
            <li><a href="#" data-key="11" class="month">Ноябрь</a></li>
            <li><a href="#" data-key="12" class="month">Декабрь</a></li>
        </ul>
        
    </nav>
    
    
<h2>НОВОСТИ</h2>
<br>

<div id="data-ajax">
    

<?php    
/* Инициализация БД */

    $host = 'localhost'; //имя хоста, на локальном компьютере это localhost
    $user = 'root'; //имя пользователя, по умолчанию это root
    $password = ''; //пароль, по умолчанию пустой
    $db_name = 'news'; //имя базы данных
    $id =0; // Id
    $title = ''; // Название статьи
    $summary = ''; // Краткое содежрание
    $content = ''; // Полное содержание
    $author = ''; // Автор
    $date = '2017-01-01 01:01:01'; // Дата, время
    $pp = 5; //Максимальное кол-во новостей на странице
    $prl = 3; // Кол-во страниц отображаемых в пагинаторе справа и слева
    $pn = 1; // Страница по умолчанию


/* Подключенние, проверка */

    $link = mysqli_connect ($host, $user, $password, $db_name);
    if (!$link){
        exit ('<br>Невозможно подключиться к базе.<br>');
    }

/* Установка параметров пагинации*/

    if (empty($pc)){
        $result_for_count = $link -> query ("SELECT COUNT(id) FROM article");
        $ccount = mysqli_fetch_array($result_for_count);
        $pc = ceil ($ccount [0]/$pp);
    } else{
        exit ('<br>Невозможно определить количество записей в базе.<br>');    
    }
?>
    
<?php
/* Расчет значений пагинатора */

    if ($pn > $prl && $prl < $pc-$pn){
        $first = $pn-$prl;
        $last = $pn+$prl;
    }elseif($pn>$prl && $prl >= $pc-$pn){
        $first = $pn-$prl;
        $last = $pc;
    }elseif($pn<=$prl && $prl < $pc-$pn){
        $first = 1;
        $last = $pn+$prl;
    }else{
        $first = 1;
        $last = $pc;
    }

?>

<!-- Пагинатор-->

    <nav>
      <ul class="pagination">
    <?php 
    for ($i=$first;$i<=$last;$i++):?>
         <li><a href="#" data-key="<?= $i?>" <?= $i==$pn?' class="active-page"':' class="page"' ?>> <?= $i ?></a></li>
    <?php endfor;  
        if ($pc>$last): ?>
        <li><a href="#" data-key="<?= ++$last?>" class="page">Далее</a></li>
    <?php endif; ?>
      </ul>
    </nav>

<?php
    

/* Установка кодировки utf8 */

    $link -> set_charset ('utf8');

    /* Запрос данных */
    $query = "SELECT id, title, summary, date FROM article ORDER BY date DESC LIMIT ".($pn-1)*$pp.",".$pp;
    $result = $link -> query ($query);
        
/* Преобразование в массив */

    for ($data = []; $row = mysqli_fetch_assoc($result); $data[] = $row){}

    foreach ($data as $key => $value){
        echo '<div class="overview">';
    //    echo '<div class="overview_title">'.$value['title'].'</div>';
        echo '<a href="'.'article.php?id='.$value['id'].'"'.'class="overview_summary" method="GET">'.$value['summary'].'</a>';
        echo '<div class="overview_date">'.$value['date'].'</div>';
    //    echo '<div class="td_info"><span class="td_title">'.$value['title'].'</span>'. '<span class="td_date">'.$value['date'].'</span>'.'</div>';
    //    echo '<div class="td_info"><span class="td_summary">'.$value['summary'].'</span></div>';
    //    echo '<hr><br>';
    //    echo '<div class="td_text">'.nl2br(htmlspecialchars($value['content'],ENT_QUOTES)).'</div>';
    //    echo '<div class="td_id">'.$value['id'].'</div>'.'<div class="td_id">'.$value['author'].'</div>';
        echo '</div>';
    }
        if ($link){$link -> close();}
?>

</div>
</div>

</body>
</html>





