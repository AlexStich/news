<?php

$host = 'localhost'; //имя хоста, на локальном компьютере это localhost
$user = 'root'; //имя пользователя, по умолчанию это root
$password = ''; //пароль, по умолчанию пустой
$db_name = 'news'; //имя базы данных


// Подключение к базе

    $link = new mysqli($host, $user, $password, $db_name);
    
// Проверка соединения
    
    if (!$link){
        echo "<br><br>Невозможно подключиться к базе данных <b>$db_name</b>";
        exit;
    }

// Установка кодировки utf8
    
    $link -> set_charset ('utf8');

// Преобразования запроса, экранирование
    $d = date('Y-m-d H:i:s');
    $title =  $link -> real_escape_string($_POST['title']);
    $summary = $link -> real_escape_string($_POST['summary']);
    $content = $link -> real_escape_string($_POST['content']);
    $author = $link -> real_escape_string($_POST['author']);
    
// Запись в базу

    $link -> query ("INSERT INTO article (title, summary, content, author, date) VALUE ('$title','$summary','$content','$author','$d')");

// Первоначальное заполнение базы

//    $months = Array (1 => 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня','июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
//    for ($m=1 ; $m<=12; $m++){
//        for ($i=1 ; $i<=31 ; $i++){
//    $query = ("INSERT INTO `article`(`title`, `summary`, `content`, `author`, `date`) VALUE "
//            . "('Новости ".$months[$m]
//            ."','Краткое содержание ".$months[$m]."  день - ".$i
//            ."','dvsdfwercxvxcvdfwerwersdsd','Мелехов П.П.','2017-".$m
//            ."-".($i>28 ? "28": $i)
//            ." 01:10:10')");
//    $link -> query ($query);
//    };
//    }
    
    // Закрыть соединение
    
    $link ->close();


// Переход на главную страницу
    header('Location: http://news/index.php');
    exit;
