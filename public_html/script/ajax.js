$(document).ready(function(){
   
// Обработчик кнопок по месяцам

   $('.menu-main').on('click','.month',function(){
      var month = $(this).attr('data-key');
      document.cookie = "month="+month;
      $('#data-ajax').load('requery.php?month='+month+'&page=1');
      });

// Обработчик кнопок пагинации

   $('#data-ajax').on('click','.page, .active-page',function(){
      var page = $(this).attr('data-key');
      
      function get_cookie ( cookie_name ){
        var results = document.cookie.match ( '(^|;) ?' + cookie_name + '=([^;]*)(;|$)' );
          if ( results )
        return ( unescape ( results[2] ) );
         else
        return null;
         }
         
      var month = get_cookie ("month");
      $('#data-ajax').load('requery.php?month='+month+'&page='+page);
      });      

});       
      
      
