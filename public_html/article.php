<!DOCTYPE html>
<html>
    
<head>
<title>Новости Екатеринбурга</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="Description" content="News">
<link rel="stylesheet" href="css/style.css">
</head>

<body>
     <nav>
        <ul class="menu-main">
            <li><a href="index.php" class="current">Главная</a></li>
        </ul>
        
    </nav>
    
<h2>НОВОСТЬ</h2>
<br>


<?php    
/* Инициализация БД */

$host = 'localhost'; //имя хоста, на локальном компьютере это localhost
$user = 'root'; //имя пользователя, по умолчанию это root
$password = ''; //пароль, по умолчанию пустой
$db_name = 'news'; //имя базы данных
$id = (empty($_GET['id']) ? 1 : $_GET['id']); // Id
$title = ''; // Название статьи
$summary = ''; // Краткое содежрание
$content = ''; // Полное содержание
$author = ''; // Автор
$date = '2017-01-01 01:01:01'; // Дата, время

/* Подключенние, проверка */

    $link = mysqli_connect ($host, $user, $password, $db_name);
    if (!$link){
    echo '<br>Невозможно подключиться к базе.<br>';
} 
    
/* Установка кодировки utf8 */

    $link -> set_charset ('utf8');

/* Запрос данных */

    $query= "SELECT * FROM article"." WHERE id=".$id." ORDER BY date";
    $result = $link -> query ($query);

/* Преобразование в массив */

    for ($data = []; $row = mysqli_fetch_assoc($result); $data[] = $row){}

    foreach ($data as $key => $value){
        echo '<div class="view">';
        echo '<div class="view_title">'.$value['title'].'</div>';
        echo '<div class="view_summary">'.$value['summary'].'</div>';
        echo '<div class="view_content">'.nl2br(htmlspecialchars($value['content'],ENT_QUOTES)).'</div>';
        echo '<div class="view_author">'.$value['author'].'</div>';
        echo '<div class="view_date">'.$value['date'].'</div>';
        echo '</div>';
    }
    if ($link){$link -> close();}
    ?>

</body>
</html>

